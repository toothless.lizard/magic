#!/usr/bin/env bash

export LC_ALL=C

cat <<EOF
+-+-+-+-+ +-+-+-+-+-+-+-+ +-+-+-+
|b|a|s|h| |n|e|t|c|a|l|c| |v|.|1|
+-+-+-+-+ +-+-+-+-+-+-+-+ +-+-+-+
EOF

read -p "enter client network: " subnet
IFS=. read o1 o2 o3 o4 <<< "$subnet"
read -p "enter client mask: " netmask
IFS=. read m1 m2 m3 m4 <<< "$netmask"

network=$((o1 & m1)).$((o2 & m2)).$((o3 & m3)).$((o4 & m4))
hostmin=$((o1 & m1)).$((o2 & m2)).$((o3 & m3)).$(((o4 & m4)+1))
hostmax=$((o1 & m1 | 255-m1)).$((o2 & m2 | 255-m2)).$((o3 & m3 | 255-m3)).$(((o4 & m4 | 255-m4)-1))
broadcast=$((o1 & m1 | 255-m1)).$((o2 & m2 | 255-m2)).$((o3 & m3 | 255-m3)).$((o4 & m4 | 255-m4))
wildcard_mask=$((255-m1)).$((255-m2)).$((255-m3)).$((255-m4))
mask2cidr ()
{
   local x=${1##*255.}
   set -- 0^^^128^192^224^240^248^252^254^ $(( (${#1} - ${#x})*2 )) ${x%%.*}
   x=${1%%$3*}
   echo $(( $2 + (${#x}/4) ))
}
cidr=$(mask2cidr $netmask)
hosts=$(((2**((32 - $cidr))) - 2))

printf "\nAddress: %s\nNetmask: %s = %s\nWildcard: %s\nNetwork: %s/%s\nHostmin: %s\nHostmax: %s\nBroadcast: %s\nHosts: %s\n\n" "$subnet" "$netmask" "$cidr" "$wildcard_mask" "$network" "$cidr" "$hostmin" "$hostmax" "$broadcast" "$hosts"
